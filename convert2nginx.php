<?php

include __DIR__ . '/vendor/autoload.php';

$sxgeo = new SxGeo\Geocoder('SxGeoCity.dat', SXGEO_BATCH);

$last = '';
$last_ip = '';

for ($a = 0; $a <= 255; $a++)
{
  if (in_array($a, [0, 10, 127, 169, 172, 192])) continue;

  for ($b = 0; $b <= 255; $b++)
  {

    for ($c = 0; $c <= 255; $c++)
    {
      $ip = $a . '.' . $b . '.'. $c . '.0';

      $geo = $sxgeo->getCityFull($ip);
      if ($sxgeo->getCountry($ip) === 'RU' && $geo['city']['name_ru'])
      {
        if ($last && $last != $geo['city']['name_ru'])
        {
          file_put_contents('geoips.conf', $first_ip . '-' . substr($last_ip, 0, -2) . '.255 ' . $last . ";\n", FILE_APPEND);
          $first_ip = $ip;
          $last = '';
          $last_ip = $ip;
        }
        else
        {
          if (!$first_ip)
            $first_ip = $ip;
        }

        $last = $geo['city']['name_ru'];
        $last_ip = $ip;
      }
      else
      {
        if ($last)
        {
          file_put_contents('geoips.conf', $first_ip . '-' . substr($last_ip, 0, -2) . '.255 ' . $last . ";\n", FILE_APPEND);
          $first_ip = '';
          $last = '';
          $last_ip = '';
        }

      }
    }

  }
}

